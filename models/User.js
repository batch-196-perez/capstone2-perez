const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName : {
		type: String,
		required: [true, "First name is required"]
	},
	lastName : {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNo : {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	email : {
		type: String,
		required: [true, "Email is required"]
	},
	password : {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	orders:[
		{
			totalAmount: {
				type: Number,
				required: [true, "Amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			products : [
				{
					productId:{
						type: String,
						required: [true, "Product ID is required"]
					},
					quantity:{
						type: Number,
						required: [true, "Quantity is required"]
					}
				}
			]
		}
	] 

});

module.exports = mongoose.model("User", userSchema);