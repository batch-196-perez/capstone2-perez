//Import User Model
const User = require("../models/User");
const Product = require("../models/Product");
//Import bcrypt
const bcrypt = require("bcrypt");

//import auth
const auth = require("../auth")

module.exports.registerUser = (req,res) =>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	//console.log(hashedPw); // not required

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		email: req.body.email,
		password: hashedPw
	})

	//console.log(req.body.password);
	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}



//Log in 

module.exports.logInUser =(req,res) =>{

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message: "No User Found"})
		} else {

			//password checking
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Incorrect Password"});
			}
		}

	})
}

//Set admin
module.exports.setAdmin = (req,res) => {

	console.log(req.params.userId);

	let update = {
		isAdmin : true
	}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}



//Get user details
module.exports.userDetails = (req,res) => {

	//console.log(req.headers.authorization);
	//console.log(req.user);
	//let token = req.headers.authorization

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


//Add order
module.exports.addOrder = async(req,res) => {

	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	} else {

		//console.log(Product)
		let isUserUpdated = await User.findById(req.user.id).then(user =>{

			let newOrder = {
				totalAmount : req.body.totalAmount,
				products : req.body.products
			};

			user.orders.push(newOrder);

			let x = user.orders.length-1;
			let latestOrderId = user.orders[x].id;

			 

			let products = req.body.products;
			products.forEach(function(orderDetail){
			

				console.log(orderDetail);
				//pro.findById(req.body.productId)
				Product.findById(orderDetail.productId).then(product => {

					let orderProduct = {
						userId: req.user.id,
						orderId: latestOrderId,
						quantity: orderDetail.quantity
					}
					product.orders.push(orderProduct);
					product.save().then().catch(err => err.message);
				})
			})
			return user.save().then(user=>true).catch(err => res.send(err.message))
		})
			if(isUserUpdated ===true ){
				return res.send({message: "Order Successful."})
			} else {
				return res.send({message: isUserUpdated})
			}
	}
}

/////Get User Order

module.exports.getUserOrder = (req,res) => {


	//User.find({})
	User.findById(req.user.id)
	.then(result => res.send(result.orders))
	.catch(error => res.send(error))
	//console.log(result);

}
	

module.exports.getAllOrder = (req,res) => {

	//if(req.user.isAdmin){
		User.find({isAdmin:false},{orders:1,_id:1})
		.then(result => res.send (result))
		.catch(error => res.send(error))
	//}
}
//firstName:0,lastName:0,mobileNo:0,email:0,password:0,isAdmin:0


// module.exports.getMyOrder = (req,res) => {

// 	User.findOne({
// 		products: {
// 			$elemMatch : {
// 				orderId : req.params.id
// 			}
// 		}


// 	}).select('products.$').exec(function(err,doc){
// 		return (result => res.send(result))
// 	});

// 	// .then(result => res.send(result.orders))
// 	// // User.findBy(result(req.params.id))	
// 	// // .then(result2 => res.send(result2))
// }

